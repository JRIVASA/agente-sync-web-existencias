﻿using AgentSyncWebExistencia.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Net;

namespace AgentSyncWebExistencia.Database_Sync_Classes
{
    class DBSync
    {

        public static Boolean CheckFTPAccess(String HostName, ref String User, ref String Pass, String RemoteFilePath, String Port = "")
        {
            try
            {

                ftp Access = new ftp(HostName, User, Pass, Properties.Settings.Default.FTP_TimeOut);

                Boolean Success = false;

                // Test connection success
                
                Success = Access.CheckFTPAccess("/");

                User = Access.ReturnFTPUser();
                Pass = Access.ReturnFTPPwd();

                Access = null;

                return Success;
            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Verificando Conexión al Servidor Remoto (FTP) [" + HostName + "]");
                return false;
            };

        }

        public static Boolean UploadFile(String HostName, String User, String Pass, String LocalFilePath, String RemoteFilePath, String Port = "",int TimeOut=6000)
        {
            try
            {
                ftp Access = new ftp(HostName, User, Pass, TimeOut);

                // Test connection success
                //object TMP = Access.directoryListSimple("/");

                Access.delete (RemoteFilePath);
                Boolean Success = Access.upload(RemoteFilePath, LocalFilePath);

                Access = null;

                return Success;
            }
            catch (System.Exception Any) {
                Program.Logger.EscribirLog(Any, "Subiendo archivo [" + RemoteFilePath + "] al servidor.");
                return false;
            };

        }

        private static void RecuperacionInicialDeCredenciales()
        {
            
            dynamic mClsTmp = null;

            Program.mUserCnSucursal = Properties.Settings.Default.LocalDB_SQLUser;
            Program.mPassCnSucursal = Properties.Settings.Default.LocalDB_SQLPass;
    
            if (!(String.Equals(Program.mUserCnSucursal, "SA", StringComparison.OrdinalIgnoreCase) && Program.mPassCnSucursal.Length == 0))
            {
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mUserCnSucursal = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mUserCnSucursal);
                    Program.mPassCnSucursal = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mPassCnSucursal);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mUserCnWeb = Properties.Settings.Default.RemoteDB_SQLUser;
            Program.mPassCnWeb = Properties.Settings.Default.RemoteDB_SQLPass;

            if (!(String.Equals(Program.mUserCnWeb, "SA", StringComparison.OrdinalIgnoreCase) && Program.mPassCnWeb.Length == 0))
            {
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mUserCnWeb = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mUserCnWeb);
                    Program.mPassCnWeb = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mPassCnWeb);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mUserFTP = Properties.Settings.Default.FTP_User;
            Program.mPassFTP = Properties.Settings.Default.FTP_Pwd;

            //if (!(String.Equals(Program.mUserFTP, "SA", StringComparison.OrdinalIgnoreCase) && Program.mPassFTP.Length == 0))
            //{
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mUserFTP = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mUserFTP);
                    Program.mPassFTP = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mPassFTP);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            //}

        }

        private static SqlConnection ConexionSucursal()
        {

            SqlConnection mCnLocal = null;

        Retry:

            try
            {
                
                mCnLocal = (Properties.Settings.Default.LocalDB_WindowsAuth ?
                Functions.getAlternateTrustedConnection(Properties.Settings.Default.LocalDB_SQLServerName, Properties.Settings.Default.LocalDB_SQLDBName, 30) :
                Functions.getAlternateConnection(Properties.Settings.Default.LocalDB_SQLServerName, Properties.Settings.Default.LocalDB_SQLDBName, 
                Program.mUserCnSucursal, Program.mPassCnSucursal, 30));
                mCnLocal.Open();
            }
            catch (SqlException SQLAny)
            {

                if (SQLAny.Number == 18456 && SQLAny.ErrorCode == -2146232060)
                {
                    
                    dynamic mClsTmp = null;
        
                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");
        
                    if (mClsTmp == null) goto Otros;
        
                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión al Servidor de la Sucursal no estan establecidos o son incorrectos. Se le solicitarán a continuación.");
        
                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);
        
                    if  (TmpVar.Length > 0)
                    {
                        Program.mUserCnSucursal=TmpVar[0];
                        Program.mPassCnSucursal=TmpVar[1];
                        Properties.Settings.Default.LocalDB_SQLUser = TmpVar[2];
                        Properties.Settings.Default.LocalDB_SQLPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog( "Los datos de acceso para la conexión al servidor son incorrectos, Conectando a " + Properties.Settings.Default.LocalDB_SQLServerName);
                    }
        
                }

                Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al servidor de la sucursal.");
                Console.WriteLine(SQLAny);

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Estableciendo conexión al servidor de la sucursal.");
                Console.WriteLine(Any);
            }

            if (mCnLocal.State != ConnectionState.Open)
            {
                Program.Logger.EscribirLog("No se pudo establecer conexión al servidor de la sucursal.");
                System.Environment.Exit(0);
            }

            return mCnLocal;

        }

        public static void ActualizarDepoProd()
        {

            Properties.Settings.Default.MultiSucursal = (Properties.Settings.Default.Sucursal.Equals(String.Empty)
            || Properties.Settings.Default.Sucursal.Equals("*"));
            Properties.Settings.Default.Clean();

            String addQueryVentasPOS = "";

            try
            {

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3);
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                RecuperacionInicialDeCredenciales();

                SqlConnection mCnLocal = ConexionSucursal();

                String TmpHost; String TmpUser; String TmpPwd; String TmpDir; String TmpPort; String TmpUrl; String TmpResp;
                GetFTPDetails(out TmpHost, out TmpUser, out TmpPwd, out TmpDir, out TmpPort);

                SqlCommand mSql = null; int Rows = 0;
                
                mSql = Functions.getParameterizedCommand(
                "IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MA_DEPOPROD_WEB')" + Functions.NewLine() + 
                "BEGIN" + Functions.NewLine() +
                Functions.Tab() + "CREATE TABLE MA_DEPOPROD_WEB" + Functions.NewLine() +
                Functions.Tab() + "(c_Sucursal NVARCHAR(10) NOT NULL," + Functions.NewLine() +
                Functions.Tab() + "c_CodArticulo NVARCHAR(20) NOT NULL," + Functions.NewLine() +
                Functions.Tab() + "n_Cantidad FLOAT NOT NULL DEFAULT 0," + Functions.NewLine() +
                (Properties.Settings.Default.PrecioPorSucursal_Aplica ? 
                Functions.Tab() + "n_Precio1 FLOAT NOT NULL DEFAULT 0," + Functions.NewLine() :
                String.Empty) +
                ") END", mCnLocal, null    
                );

                Rows = mSql.ExecuteNonQuery();

                mSql = Functions.getParameterizedCommand("DELETE DW" + Functions.NewLine() + 
                "FROM MA_DEPOPROD D" + Functions.NewLine() + 
                "INNER JOIN MA_DEPOSITO DP ON (D.c_CodDeposito = DP.c_CodDeposito)" + Functions.NewLine() + 
                "LEFT JOIN MA_PRODUCTOS_WEB PW ON (D.c_CodArticulo = PW.Codigo)" + Functions.NewLine() + 
                "INNER JOIN MA_DEPOPROD_WEB DW ON (DP.c_CodLocalidad = DW.c_Sucursal) AND (D.c_CodArticulo = DW.c_CodArticulo)"
                , mCnLocal, null
                );

                Rows = mSql.ExecuteNonQuery();
            
                Double mExistencia = 1; String mRutaDestino; String mRutaLocal;
                
                if (Properties.Settings.Default.MultiSucursal)
                {
                    mRutaDestino = "AGENT_MULTI_LOCALIDAD.txt";
                    mRutaLocal = Properties.Settings.Default.RutaArchivo + mRutaDestino;
                }
                else
                {
                    mRutaDestino = "AGENT_" + Properties.Settings.Default.Sucursal + ".txt";
                    mRutaLocal = Properties.Settings.Default.RutaArchivo + mRutaDestino;
                }

                if (Properties.Settings.Default.PorcentajeExistencia < 100)
                    mExistencia = (Convert.ToDouble(Properties.Settings.Default.PorcentajeExistencia) / 100);

                String SQLExtra = "SELECT TB1.*, P.nu_StockMin AS StockMinimo, P.Cant_Decimales AS CantDec" + 
                (Properties.Settings.Default.PrecioPorSucursal_Aplica ? ", P.n_Precio1" : String.Empty) + 
                " FROM (";

                String CualesDepositos = String.Empty;

                if (!Properties.Settings.Default.SoloEstosDepositos.isUndefined())
                {
                    String[] ListaDepositos;
                    String[] Separador = new String[] {"|"};

                    ListaDepositos = Properties.Settings.Default.SoloEstosDepositos.Split(Separador, StringSplitOptions.None);

                    foreach (String Dep in ListaDepositos)
                    {
                        CualesDepositos += "'" + Dep + "'" + ", ";
                    }

                    CualesDepositos = CualesDepositos.Remove(CualesDepositos.Length - ", ".Length);

                    CualesDepositos = " AND DP.c_CodDeposito IN (" + CualesDepositos + ")";
                }
                else if (!Properties.Settings.Default.TodosMenosEstosDepositos.isUndefined())
                {
                    String[] ListaDepositos;
                    String[] Separador = new String[] { "|" };

                    ListaDepositos = Properties.Settings.Default.TodosMenosEstosDepositos.Split(Separador, StringSplitOptions.None);

                    foreach (String Dep in ListaDepositos)
                    {
                        CualesDepositos += "'" + Dep + "'" + ", ";
                    }

                    CualesDepositos = CualesDepositos.Remove(CualesDepositos.Length - ", ".Length);

                    CualesDepositos = " AND DP.c_CodDeposito NOT IN (" + CualesDepositos + ")";
                }

                String SQLUno = "SELECT DP.c_CodLocalidad AS CodLocalidad, D.c_CodArticulo AS CodArticulo," + Functions.NewLine() +
                "CASE WHEN D.n_Cant_Comprometida < 0 THEN 0 ELSE D.n_Cant_Comprometida END AS CantComprometida," + Functions.NewLine() +
                "D.n_Cantidad AS Existencia, 0 AS VentasPOS" + Functions.NewLine() + 
		        "FROM MA_DEPOPROD D" + Functions.NewLine() +
                "INNER JOIN dbo.MA_DEPOSITO DP ON D.c_CodDeposito = DP.c_CodDeposito" + CualesDepositos +
                (!Properties.Settings.Default.MultiSucursal ? " AND DP.c_CodLocalidad = @Sucursal" : String.Empty) + Functions.NewLine() +
		        "INNER JOIN dbo.MA_PRODUCTOS_WEB PW ON (D.c_CodArticulo = PW.Codigo)" + Functions.NewLine() +
		        "INNER JOIN MA_PRODUCTOS MP ON (D.c_CodArticulo  = MP.C_CODIGO)" + Functions.NewLine() +
                "GROUP BY DP.c_CodLocalidad, D.c_CodArticulo, D.n_Cant_Comprometida, D.n_Cantidad" + Functions.NewLine() + Functions.NewLine();

                if (!Properties.Settings.Default.ConsideraVentasPOS)
                {
                    addQueryVentasPOS = Functions.NewLine() + "WHERE 1 = 2";
                }
                else
                {
                    addQueryVentasPOS = Functions.NewLine() + "WHERE CAST(f_Fecha AS DATE) = CAST(GETDATE() AS DATE)";
                }

                String SQLDos = "UNION" + Functions.NewLine() + Functions.NewLine() + 
		        "SELECT " +
                (Properties.Settings.Default.MultiSucursal || Properties.Settings.Default.IncluirSucursalEnArchivo ? "c_Localidad" : "''") +
                " AS CodLocalidad, VAD20.Cod_Principal AS CodArticulo, 0 AS Existencia," + Functions.NewLine() +
		        "0 AS CantComprometida, VAD20.Cantidad AS VentasPOS" + Functions.NewLine() +
                "FROM [VAD20].[DBO].[MA_TRANSACCION] VAD20" + addQueryVentasPOS + Functions.NewLine() +
                "GROUP BY c_Localidad, Cod_Principal, f_Fecha, Cantidad" + Functions.NewLine() + Functions.NewLine();

                String SQLTresHeader = "SELECT " +
                (Properties.Settings.Default.MultiSucursal ? "CodLocalidad" : "@Sucursal") + " AS c_CodLocalidad, " +
                "CodArticulo, " + Functions.NewLine() +
                "CASE WHEN ROUND((((Existencia - CantComprometida) * (@Porcentaje))" +
                (Properties.Settings.Default.ConsideraStockMinimo ? " - StockMinimo": String.Empty) +
                " - VentasPOS), CantDec, 1) <= 0 THEN 0" + Functions.NewLine() +
                "ELSE CASE WHEN Existencia > 0 THEN ROUND(((Existencia - CantComprometida) * (@Porcentaje)" + 
                (Properties.Settings.Default.ConsideraStockMinimo ? " - StockMinimo": String.Empty) +
                " - VentasPOS), CantDec, 1) ELSE 0 END END AS Resultado" +
                (Properties.Settings.Default.PrecioPorSucursal_Aplica ? ", n_Precio1" : String.Empty) + 
                Functions.NewLine() +
                "FROM (" + SQLExtra + " " + Functions.NewLine() + Functions.NewLine() +
                "SELECT CodLocalidad, CodArticulo, " + 
                "SUM(Existencia) AS Existencia, SUM(CantComprometida) AS CantComprometida, SUM(VentasPOS) AS VentasPOS FROM (" + Functions.NewLine() + Functions.NewLine();
                
                String SQLExtraDos = ") AS TB1 INNER JOIN MA_PRODUCTOS P ON TB1.CodArticulo = P.c_Codigo";

                String SQLTresFooter =  ") AS T" + Functions.NewLine() + Functions.NewLine() +
                "GROUP BY T.CodLocalidad, T.CodArticulo " + Functions.NewLine() + Functions.NewLine() + SQLExtraDos + " " + Functions.NewLine() + Functions.NewLine() +
                ") AS TBFinal " + Functions.NewLine() +
                "GROUP BY TBFinal.CodLocalidad, TBFinal.Existencia, TBFinal.CantComprometida, TBFinal.CodArticulo, TBFinal.StockMinimo, TBFinal.VentasPOS, TBFinal.CantDec" +
                (Properties.Settings.Default.PrecioPorSucursal_Aplica ? ", TBFinal.n_Precio1" : String.Empty) +
                String.Empty;

                String SQL = SQLTresHeader + SQLUno + SQLDos + SQLTresFooter;

                SQL = "INSERT INTO MA_DEPOPROD_WEB" + Functions.NewLine() + SQL;

                mSql = Functions.getParameterizedCommand(SQL
                , mCnLocal, new SqlParameter[]{ new SqlParameter(){ ParameterName = "@Porcentaje", Value = mExistencia },
                new SqlParameter(){ ParameterName = "@Sucursal", Value = Properties.Settings.Default.Sucursal }
                });

                Rows = mSql.ExecuteNonQuery();

                mSql = Functions.getParameterizedCommand("SELECT * FROM MA_DEPOPROD_WEB " + Functions.NewLine() +
                (!Properties.Settings.Default.MultiSucursal ? " WHERE c_Sucursal = @Sucursal" : String.Empty) + Functions.NewLine() +
                "ORDER BY c_Sucursal, c_CodArticulo" + Functions.NewLine(), 
                mCnLocal, new SqlParameter[]{ new SqlParameter(){ ParameterName = "@Sucursal", Value = Properties.Settings.Default.Sucursal }}
                );

                SqlDataReader mRs = mSql.ExecuteReader();

                try { System.IO.File.Delete(mRutaLocal); } catch {}

                System.IO.StreamWriter TmpStream = null;

                try { TmpStream = System.IO.File.AppendText(mRutaLocal); }
                catch  { }
                
                while (mRs.Read() && TmpStream != null)
                {
                    try { 
                        
                        String TmpLn = String.Empty;

                        if (Properties.Settings.Default.MultiSucursal || Properties.Settings.Default.IncluirSucursalEnArchivo)
                            TmpLn += mRs["c_Sucursal"].ToString() + "|";

                        TmpLn += mRs["c_CodArticulo"].ToString();
                        TmpLn += "|" + Math.Truncate(Convert.ToDouble(mRs["n_Cantidad"])).ToString();

                        if (Properties.Settings.Default.PrecioPorSucursal_Aplica)
                            TmpLn += "|" + Convert.ToDouble(mRs["n_Precio1"]).ToString();

                        TmpStream.WriteLine(TmpLn);

                    }
                    catch
                    (System.Exception Any) {
                        Program.Logger.EscribirLog(Any, "Al insertar registro en el archivo de existencias.");
                    }
                }

                mRs.Close();
                TmpStream.Close();
                mCnLocal.Close();
                
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                
                if (UploadFile(TmpHost, TmpUser, TmpPwd, mRutaLocal, TmpDir + mRutaDestino, "21" ,Properties.Settings.Default.FTP_TimeOut))
                {

                    try
                    {

                        //System.Windows.Forms.MessageBox.Show("Archivo subido. Enviando a carpeta Enviado.");

                        if (!System.IO.Directory.Exists(QuickSettings.FolderPath + "\\Enviado"))
                        {
                            System.IO.Directory.CreateDirectory(QuickSettings.FolderPath + "\\Enviado");
                        }

                        TmpResp = mRutaLocal.Replace(mRutaLocal, "Enviado\\" + 
                        (Properties.Settings.Default.MultiSucursal ? "MULTI-SUCURSAL" : Properties.Settings.Default.Sucursal) + "_" + 
                        DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + "_" + 
                        DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00") + 
                        ".txt");

                        //System.Windows.Forms.MessageBox.Show("Archivo a Copiar a Enviado:\n" + 
                        //"Origen: " + mRutaLocal + " \n" + 
                        //"Destino: " + TmpResp);

                        System.IO.File.Copy(mRutaLocal, TmpResp);

                    }
                    catch (System.Exception Any) { Program.Logger.EscribirLog(Any, "Moviendo archivos a Carpeta Enviados"); }

                }

                //System.Windows.Forms.MessageBox.Show("Llamando a Procesar.");

                if (Properties.Settings.Default.MultiSucursal)
                    TmpUrl = Properties.Settings.Default.FTP_ProcessURL + "?Sucursal=*";
                else
                    TmpUrl = Properties.Settings.Default.FTP_ProcessURL + "?Sucursal=" + Properties.Settings.Default.Sucursal;

                TmpResp = Functions.HttpPost(TmpUrl, "");

                try
                {
                    //System.Windows.Forms.MessageBox.Show("Archivo procesado. Borrando Temporal.");
                    System.IO.File.Delete(mRutaLocal);
                }
                catch (Exception Any)
                {
                    Program.Logger.EscribirLog(Any, "Borrando archivo temporal [" + mRutaLocal + "]");
                }

            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "ActualizarDepoProd(). Construyendo archivo de actualización.");
            }

        }

        private static void GetFTPDetails(out String HostName, out String User, out String Pwd, out String DestDir, out String Port)
        {

            HostName = "";
            User = "";
            Pwd = "";
            DestDir = "";
            Port = "";

            try
            {

                if (Properties.Settings.Default.FTP_Auto)
                {
                    SqlConnection mCnLocal = Functions.getAlternateTrustedConnection(Properties.Settings.Default.FTP_AutoInstance, Properties.Settings.Default.FTP_AutoDB, 30);
                    mCnLocal.Open();

                    SqlCommand mSql = null;

                    mSql = Functions.getParameterizedCommand("SELECT * FROM TmpSettings", mCnLocal, null);

                    SqlDataReader mRs = mSql.ExecuteReader();

                    while (mRs.Read())
                    {

                        switch (mRs["Field"].ToString())
                        {
                            case "FTP_Host":
                                HostName = mRs["Value"].ToString();
                                break;
                            case "FTP_User":
                                User = mRs["Value"].ToString();
                                break;
                            case "FTP_Pwd":
                                Pwd = mRs["Value"].ToString();
                                break;
                            case "FTP_Dir":
                                DestDir = mRs["Value"].ToString();
                                break;
                            case "FTP_Port":
                                Port = mRs["Value"].ToString();
                                break;
                        }

                    }

                    mRs.Close();
                    mCnLocal.Close();

                }
                else
                {
                    HostName = Properties.Settings.Default.FTP_Host;
                    //User = Properties.Settings.Default.FTP_User;
                    //Pwd = Properties.Settings.Default.FTP_Pwd;
                    User = Program.mUserFTP;
                    Pwd = Program.mPassFTP;
                    if (User == null) User = String.Empty;
                    if (Pwd == null) Pwd = String.Empty;

                    DestDir = Properties.Settings.Default.FTP_Dir;
                    Port = Properties.Settings.Default.FTP_Port;
                }
            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Error recuperando credenciales FTP");
            }

            if (!CheckFTPAccess(HostName, ref User, ref Pwd, DestDir))
            {
                Program.Logger.EscribirLog("Error temporal al intentar conectar al servidor FTP [" + HostName + "]. Verifique el acceso al servidor FTP y las credenciales.");
                System.Environment.Exit(0);
            }

        }

    }
    
}
